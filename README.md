#                           Terraform for Infrastructure as Code using Gitlab

## Terraform state in Google Cloud Storage

To work on our infrastructure with a team, we can use source control to share our infrastructure code. By default, Terraform stores the state of our infrastructure in a local state file. We could commit it with our infrastructure code, but the best practice for sharing a Terraform state when working with teams is to store it in remote storage. In our case, we will configure Terraform to store the state in a Google Cloud Storage Bucket.

![](remote-state-1.png)

## Create the GKE cluster
We should now have a **terraform.tf** file and the service account key-file in our working directory. Here is our target directory structure:

```
.
├── .terraform
│   ├── modules
│   │   ├── gke
│   │   └── modules.json
│   └── plugins
│       └── ...
├── main.tf
├── providers.tf
├── terraform-gke-keyfile.json
├── terraform.tf
├── variables.tf
└── variables.auto.tfvars
```

The **.terraform/** directory is created and managed by Terraform, this is where it stores the external modules and plugins we will reference. To create the GKE cluster with Terraform, we will use the Google Terraform provider and a GKE community module. A module is a package of Terraform code that combines different resources to create something more complex. In our case, we will use a single module that will create for us many various resources such as a Google Container cluster, node pools, and a cluster service account if necessary.

To configure terraform to communicate with the Google Cloud API and to create GCP resources, we use **providers.tf** file.
The following **main.tf** file is where we reference the modules and provide it with the appropriate variables **variables.tf** file.

The following variables.auto.tfvars file is to specify values for the variables defined in **variables.tf**:


