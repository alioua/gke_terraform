# https://www.terraform.io/docs/providers/random/r/id.html
provider "random" {
  version = "2.1.1"
}

resource "random_id" "entropy" {
  byte_length = 6
}

# https://www.terraform.io/docs/providers/google/r/google_service_account.html
resource "google_service_account" "cluster_service_account" {
  project      = var.gcp_project_id
  account_id   = "tf-gke-${random_id.entropy.hex}"
  display_name = "Terraform-managed service account for cluster ${var.cluster_name}"
}

# https://www.terraform.io/docs/providers/google/r/google_project_iam.html
resource "google_project_iam_member" "cluster_service_account-log_writer" {
  role    = "roles/logging.logWriter"
  member  = "serviceAccount:${google_service_account.cluster_service_account.email}"
}


resource "google_project_iam_member" "monitoring-metric-writer" {
  role   = "roles/monitoring.metricWriter"
  member  = "serviceAccount:${google_service_account.cluster_service_account.email}"
}

resource "google_project_iam_member" "monitoring-viewer" {
  role   = "roles/monitoring.viewer"
  member  = "serviceAccount:${google_service_account.cluster_service_account.email}"
}

resource "google_project_iam_member" "storage-object-viewer" {
  role   = "roles/storage.objectViewer"
  member  = "serviceAccount:${google_service_account.cluster_service_account.email}"
}