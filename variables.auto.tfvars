cluster_name = "gitlab-terraform-gke"
max_node_count = "5"
min_node_count = "1"

# The location (region or zone) in which the cluster will be created. If you
# specify a zone (such as us-central1-a), the cluster will be a zonal cluster.
# If you specify a region (such as us-west1), the cluster will be a regional
# cluster.
gcp_location = "europe-west1"

daily_maintenance_window_start_time = "03:00"

vpc_network_name = "gitlab-vpc"

vpc_subnetwork_name = "gitlab-vpc"

vpc_subnetwork_cidr_range = "10.0.16.0/20"

cluster_secondary_range_name = "pods"

cluster_secondary_range_cidr = "10.16.0.0/12"

services_secondary_range_name = "services"

services_secondary_range_cidr = "10.1.0.0/20"

master_ipv4_cidr_block = "172.16.0.0/28"

access_private_images = "false"

http_load_balancing_disabled = "false"